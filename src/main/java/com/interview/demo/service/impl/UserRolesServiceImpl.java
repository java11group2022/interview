package com.interview.demo.service.impl;

import com.interview.demo.model.UserRoles;
import com.interview.demo.repository.UserRoleRepository;
import com.interview.demo.service.UserRolesService;
import org.springframework.stereotype.Service;


import java.util.List;

@Service
public class UserRolesServiceImpl implements UserRolesService {

  private final UserRoleRepository userRoleRepository;

  public UserRolesServiceImpl(UserRoleRepository userRoleRepository) {
    this.userRoleRepository = userRoleRepository;
  }

  @Override
  public List<UserRoles> getAllUserRoles(Long userId) {
    return userRoleRepository.findAllByUserId(userId);
  }
}
