//package com.interview.demo.interview.tasks;
//
///*
//Как изменить порядок элементов в строке на обратный без использования вспомогательных классов?
//*/
//public class Task5 {
//
//    public static void main(String[] args) {
//        String string = "Java test";
//        System.out.println(solution(string));
//    }
//
//    public static String solution(String str) {
//        String s = "";
//        for (int i = str.length() - 1; i >= 0; i--) {
//            s += str.charAt(i);
//        }
//        return s;
//    }
//
//    public static String reverseByArray(String s) {
//        char[] a = s.toCharArray();
//        char[] b = new char[a.length];
//        for (int i = 0; i < a.length; i++) {
//            b[(a.length - 1) - i] = a[i];
//        }
//        return new String(b);
//    }
//
//    public static String reverseString(String str) {
//        if (str.isEmpty()) {
//            return str;
//        }
//        else {
//            return reverseString(str.substring(1)) + str.charAt(0);
//        }
//    }
//
//
//}
