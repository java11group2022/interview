package com.interview.demo.interview.tasks;

/*
Каким будет результат выполнения данного кода?
 */

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Task4 {
    public static void main(String[] args) {
        List<String> stringList = new ArrayList<String>();
        stringList.add("one");
        stringList.add("one and a half");
        stringList.add("two");
        stringList.add("two and a half");
        stringList.add("three and a half");
        // CopyOnWriteArrayList

        System.out.println("Before " + stringList);
        Iterator<String> stringIterator = stringList.iterator();
        while (stringIterator.hasNext()) {
            String next = stringIterator.next();
            if (next.equals("two and a half")) {
                stringList.add("three");
            }
        }

        System.out.println("After " + stringList);
    }
    /*
    Fail-fast – «быстрый» итератор. Когда после его создания коллекция как-либо изменилась,
    он падает с ошибкой без лишних разбирательств. Так работает итератор класса ArrayList,
    при изменении он выбрасывает ConcurrentModificationException.
    Рекомендуется не основывать логику программы на fail-fast отказах, и использовать их только как признак ошибки реализации.

    Fail-safe - ConcurrentHashMap
     */
}