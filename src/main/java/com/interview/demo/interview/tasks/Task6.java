//package com.interview.demo.interview.tasks;
//
///*
//Как найти средний элемент в LinkedList за один проход?
//Это один из самых популярных вопросов на собеседованиях.
//Его используют даже в телефонных интервью, чтобы быстро определить общий уровень
//знаний кандидата и оценить его способность быстро решать нестандартные задачи.
// */
//public class Task6 {
//
//    public static void main(String[] args) {
//        MyLinkedList linkedList = new MyLinkedList();
//        MyLinkedList.Node head = linkedList.head();
//
//        linkedList.add(new MyLinkedList.Node("1"));
//        linkedList.add(new MyLinkedList.Node("2"));
//        linkedList.add(new MyLinkedList.Node("3"));
//        linkedList.add(new MyLinkedList.Node("4"));
//        linkedList.add(new MyLinkedList.Node("5"));
//        linkedList.add(new MyLinkedList.Node("6"));
//        linkedList.add(new MyLinkedList.Node("7"));
//        linkedList.add(new MyLinkedList.Node("8"));
//        linkedList.add(new MyLinkedList.Node("9"));
//        linkedList.add(new MyLinkedList.Node("10"));
//
//        MyLinkedList.Node current = head;
//        int length = 0;
//        MyLinkedList.Node middle = head;
//        while (current.next() != null) {
//            length++;
//            if (length %2 == 0) {
//                middle = middle.next();
//            }
//            current = current.next();
//        }
//        if (length % 2 == 1) {
//            middle = middle.next();
//        }
//        System.out.println("Length of LinkedList: " + length);
//        System.out.println("middle element of LinkedList: " + middle);
//    }
//}
//
//class MyLinkedList {
//    private final Node head;
//    private Node tail;
//
//    MyLinkedList() {
//        this.head = new Node("head");
//        tail = head;
//    }
//
//    public Node head() {
//        return head;
//    }
//
//    public void add(Node node) {
//        tail.next = node;
//        tail = node;
//    }
//
//    public static class Node {
//        private Node next;
//        private String data;
//
//        public Node(String data) {
//            this.data = data;
//        }
//
//        public String data() {
//            return data;
//        }
//
//        public void setData(String data) {
//            this.data = data;
//        }
//
//        public Node next() {
//            return next;
//        }
//
//        public void setNext(Node next) {
//            this.next = next;
//        }
//
//        public String toString() {
//            return this.data;
//        }
//    }
//}
//
///*
//ОТВЕТ
//В этой задаче достаточно ввести два указателя. Первый будет увеличиваться при прохождении одного узла списка, второй – при прохождении двух узлов. В
//момент, когда второй указатель дойдёт до конца списка (наткнётся на NULL), первый будет указывать на середину списка.
// */
//
//
//
//
//
