//package com.interview.demo.interview.tasks;//package interviewtasks;
//
//import java.io.FileNotFoundException;
//import java.io.IOException;
//
//public class Task3 {
//    public static void main(String[] args) {
//        try {
//            foo();
//        }
//        catch (IOException e) {
//            e.printStackTrace();
//        }
//        catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
//        //answer
////        catch (IOException | FileNotFoundException e) {
////
////        }
//    }
//
//    public static void foo () throws IOException, FileNotFoundException {
//
//    }
//}
