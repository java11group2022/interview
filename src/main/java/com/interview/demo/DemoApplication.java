package com.interview.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/*
Задание нужно выполнить используя Spring security framework
Нужно написать WEB сервис реализующий REST API
У сервиса должен быть метод login, который получает username и password и отдает набор
permissions доступных пользователю.
У сервиса есть еще некоторый набор методов (test1, test2, test3)
У каждого из этих методов есть соответствующий permission.
Нужно реализовать annotation, который можно было бы навесить на методы test1, test2,
test3, чтобы методы исполнялись только в том случае если у пользователя есть
соответствующий permission.
Если permission-а у пользователя нет, то общий код (снаружи от методов test1, test2, test3)
должен генерировать соответствующее сообщение об ошибке и возвращать его через
REST ответ.
 */
@SpringBootApplication
public class DemoApplication {
    
    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }
}
